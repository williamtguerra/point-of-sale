# point-of-sale

## About

Simple point of sale (POS) app based on this [example API](https://github.com/AveroLLC/check-api).

Created with [simple-react-full-stack](https://github.com/crsandeep/simple-react-full-stack) boilerplate.

## Quick Start

**note:** you need to add the authorization key in `src/server/auth.json` to make successful API calls

```bash
# Clone the repository
git clone https://gitlab.com/williamtguerra/point-of-sale.git

# Go inside the directory
cd point-of-sale

# Install dependencies
yarn

# Start development server
# (recommended for a quick end-to-end demo)
yarn dev

# Build for production
yarn build

# Start production server
yarn start
```

If your browser does not automatically open a new tab with the app, navigate to http://localhost:3000/
