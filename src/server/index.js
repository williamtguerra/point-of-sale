const express = require('express');
const fetch = require('isomorphic-fetch');
const auth = require('./auth.json');

const app = express();
const apiUrl = 'https://check-api.herokuapp.com';

const options = {
    headers: {
        authorization: auth.key
    }
};

const tableMap = {};
const itemMap = {};

const mapChecks = (checks) => {
    const closedChecks = [];
    const openChecks = [];

    checks.forEach((c) => {
        const check = c;
        check.tableNumber = tableMap[c.tableId] ? tableMap[c.tableId].number : -1;

        if (c.closed) {
            closedChecks.push(check);
        } else {
            openChecks.push(check);
        }
    });
    return { closedChecks, openChecks };
};

const mapData = (rawData) => {
    const { tables, items } = rawData;

    tables.forEach((table) => {
        tableMap[table.id] = table;
    });

    items.forEach((item) => {
        itemMap[item.id] = item;
    });

    const checks = mapChecks(rawData.checks);

    return {
        tables,
        items,
        ...checks
    };
};

app.use(express.static('dist'));

app.get('/api/fetchInitialData', async (req, res) => {
    const responses = await Promise.all([
        fetch(`${apiUrl}/tables`, options),
        fetch(`${apiUrl}/items`, options),
        fetch(`${apiUrl}/checks`, options)
    ]);

    const data = await Promise.all(responses.map(r => r.json()));

    const transformedData = mapData({
        tables: data[0],
        items: data[1],
        checks: data[2]
    });

    res.send(transformedData);
});

app.get('/api/getTables', async (req, res) => {
    const response = await fetch(`${apiUrl}/tables`, options);
    const tables = await response.json();
    res.send(tables);
});

app.get('/api/getItems', async (req, res) => {
    const response = await fetch(`${apiUrl}/items`, options);
    const items = await response.json();
    res.send(items);
});

app.get('/api/getChecks', async (req, res) => {
    const response = await fetch(`${apiUrl}/checks`, options);
    const checkData = await response.json();

    res.send({ ...mapChecks(checkData) });
});

app.get('/api/getCheck', async (req, res) => {
    const { checkId } = req.query;
    console.log('Fetching check', checkId);

    const response = await fetch(`${apiUrl}/checks/${checkId}`, options);
    const check = await response.json();
    const orderedItems = [];

    check.orderedItems.forEach((i) => {
        const item = i;
        item.itemName = itemMap[i.itemId] ? itemMap[i.itemId].name.toLowerCase() : '';
        item.itemPrice = itemMap[i.itemId] ? itemMap[i.itemId].price : -1;
        orderedItems.push(item);
    });
    check.orderedItems = orderedItems;

    check.tableNumber = tableMap[check.tableId] ? tableMap[check.tableId].number : -1;

    res.send(check);
});

app.post('/api/postCheck', async (req, res) => {
    const data = { tableId: req.query.tableId };
    console.log('posting check:', data);
    const response = await fetch(`${apiUrl}/checks`, {
        ...options,
        method: 'POST',
        body: JSON.stringify(data)
    });
    res.send(response);
});

app.put('/api/closeCheck', async (req, res) => {
    const { checkId } = req.query;
    console.log('closing check:', checkId);
    const response = await fetch(`${apiUrl}/checks/${checkId}/close`, {
        ...options,
        method: 'PUT'
    });
    res.send(response);
});

app.put('/api/addItem', async (req, res) => {
    const { checkId, itemId } = req.query;
    const data = { itemId };
    console.log('Adding item to check:', data);
    const response = await fetch(`${apiUrl}/checks/${checkId}/addItem`, {
        ...options,
        method: 'PUT',
        body: JSON.stringify(data)
    });
    res.send(response);
});

app.put('/api/voidItem', async (req, res) => {
    const { checkId, orderedItemId } = req.query;
    const data = { orderedItemId };
    console.log('Adding item to check:', data);
    const response = await fetch(`${apiUrl}/checks/${checkId}/voidItem`, {
        ...options,
        method: 'PUT',
        body: JSON.stringify(data)
    });
    res.send(response);
});

app.delete('/api/deleteChecks', async (req, res) => {
    console.log('deleting all checks');
    const response = await fetch(`${apiUrl}/checks`, {
        ...options,
        method: 'DELETE'
    });
    res.send(response);
});

app.listen(process.env.PORT || 8088, () => console.log(`Listening on port ${process.env.PORT || 8088}!`));
