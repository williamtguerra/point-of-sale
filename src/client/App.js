import React from 'react';
import POS from './components/POS/POS';
import './App.css';

const App = () => (
    <div className="app">
        <POS />
    </div>
);

export default App;
