import React, { Component } from 'react';
import ActiveCheck from '../ActiveCheck/ActiveCheck';
import Items from '../Items/Items';
import NewCheckButton from '../NewCheckButton/NewCheckButton';
import './CheckEditor.css';

export default class CheckEditor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            check: {}
        };
    }

    async componentDidUpdate() {
        const { activeCheck } = this.props;
        const { check } = this.state;

        if (activeCheck && activeCheck !== check.id) {
            await this.fetchCheck();
        } else if (!activeCheck && check.id) {
            this.onCheckDismissed();
        }
    }

    onItemVoided = async (itemId) => {
        const { voidItem } = this.props;
        await voidItem(itemId);
        await this.fetchCheck();
    };

    onCheckClosed = async () => {
        const { closeCheck } = this.props;
        await closeCheck();
    };

    onCheckDismissed = () => {
        const { setActiveCheck } = this.props;
        setActiveCheck(null);
        this.setState({ check: {} });
    };

    createNewCheck = async (tableId) => {
        const { postCheck } = this.props;
        await postCheck(tableId);
    };

    fetchCheck = async () => {
        const { activeCheck } = this.props;
        const response = await fetch(`/api/getCheck?checkId=${activeCheck}`);
        const check = await response.json();
        console.log('Check fetched:', check.id.split('-')[0], check);
        this.setState({ check });
    };

    render() {
        const {
            activeCheck, addItem, bookedTables, items, tables
        } = this.props;
        const { check } = this.state;

        const showItems = check && check.closed === false;
        const showButton = !activeCheck && tables.length > 0;

        return (
            <div className="check-editor">
                <div className="active-check-window">
                    {check.id && (
                        <ActiveCheck
                            check={check}
                            onCheckClosed={this.onCheckClosed}
                            onCheckDismissed={this.onCheckDismissed}
                            onItemVoided={this.onItemVoided}
                        />
                    )}
                    {showButton && (
                        <NewCheckButton
                            bookedTables={bookedTables}
                            createNewCheck={this.createNewCheck}
                            tables={tables}
                        />
                    )}
                </div>
                <Items
                    showItems={showItems}
                    addItem={addItem}
                    fetchCheck={this.fetchCheck}
                    items={items}
                />
            </div>
        );
    }
}
