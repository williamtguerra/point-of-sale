import React, { Component } from 'react';
import CheckEditor from '../CheckEditor/CheckEditor';
import Checks from '../Checks/Checks';
import './POS.css';

export default class POS extends Component {
    state = {
        activeCheck: null,
        closedChecks: [],
        openChecks: [],
        items: [],
        tables: []
    };

    async componentDidMount() {
        await this.fetchInitialData();

        // allows us to delete checks in console
        window.deleteChecks = this.deleteChecks;
    }

    fetchInitialData = async () => {
        const response = await fetch('/api/fetchInitialData');
        const data = await response.json();
        this.setState({ ...data });
        console.log('Init Data:', data);
    };

    fetchChecks = async () => {
        const response = await fetch('/api/getChecks');
        const checks = await response.json();
        this.setState({ ...checks });
        console.log('Checks', checks);
    };

    postCheck = async (tableId) => {
        const response = await fetch(`/api/postCheck?tableId=${tableId}`, {
            method: 'POST'
        });
        const out = await response.json();
        console.log(`Posted check: tableId=${tableId}`, out);

        await this.fetchChecks();
        this.setFirstCheckActive();
    };

    closeCheck = async () => {
        const { activeCheck } = this.state;

        const response = await fetch(`/api/closeCheck?checkId=${activeCheck}`, {
            method: 'PUT'
        });
        const out = await response.json();
        console.log(`Closed check: ${activeCheck}`, out);

        this.setActiveCheck(null);
        this.fetchChecks();
    };

    addItem = async (itemId) => {
        const { activeCheck } = this.state;
        await fetch(`/api/addItem?checkId=${activeCheck}&itemId=${itemId}`, {
            method: 'PUT'
        });
        console.log(`Added item(${itemId.split('-')[0]}) to check(${activeCheck.split('-')[0]})`);
    };

    voidItem = async (orderedItemId) => {
        const { activeCheck } = this.state;
        await fetch(`/api/voidItem?checkId=${activeCheck}&orderedItemId=${orderedItemId}`, {
            method: 'PUT'
        });
        console.log(
            `Voided item(${orderedItemId.split('-')[0]}) from check(${activeCheck.split('-')[0]})`
        );
    };

    deleteChecks = async () => {
        await fetch('/api/deleteChecks', {
            method: 'DELETE'
        });

        console.log('Deleted all checks');

        this.fetchChecks();
        this.setActiveCheck(null);
    };

    setFirstCheckActive = () => {
        // hacky way to open a check that was just posted
        const { openChecks } = this.state;
        const activeCheck = openChecks.length && openChecks[0].id;
        this.setActiveCheck(activeCheck);
    };

    setActiveCheck = (id) => {
        this.setState({ activeCheck: id });
    };

    render() {
        const {
            activeCheck, closedChecks, openChecks, items, tables
        } = this.state;

        const bookedTables = openChecks.map(check => check.tableNumber);

        return (
            <div className="point-of-sale">
                <div className="container">
                    <Checks
                        activeCheck={activeCheck}
                        closedChecks={closedChecks}
                        openChecks={openChecks}
                        setActiveCheck={this.setActiveCheck}
                    />
                    <CheckEditor
                        activeCheck={activeCheck}
                        addItem={this.addItem}
                        bookedTables={bookedTables}
                        closeCheck={this.closeCheck}
                        items={items}
                        tables={tables}
                        postCheck={this.postCheck}
                        setActiveCheck={this.setActiveCheck}
                        voidItem={this.voidItem}
                    />
                </div>
            </div>
        );
    }
}
