import React, { Component } from 'react';
import './ActiveCheck.css';

export default class CheckEditor extends Component {
    renderItems() {
        const { check, onItemVoided } = this.props;

        const { closed, orderedItems } = check;
        orderedItems.sort((a, b) => (a.dateCreated < b.dateCreated ? -1 : 1));

        return (
            <table className="active-check__ordered-items">
                <tbody>
                    {orderedItems.map((item) => {
                        const className = item.voided ? 'ordered-item void' : 'ordered-item';
                        return (
                            <tr className={className} key={`item-${item.id}`}>
                                {!closed && (
                                    <td className="ordered-item__void">
                                        {!item.voided && (
                                            <button
                                                className="void-button"
                                                type="button"
                                                onClick={() => onItemVoided(item.id)}
                                            >
                                                Void Item
                                            </button>
                                        )}
                                    </td>
                                )}
                                <td className="ordered-item__name">{item.itemName}</td>

                                <td className="ordered-item__price">
                                    {`$${item.itemPrice.toFixed(2)}`}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
    }

    render() {
        const { check, onCheckClosed, onCheckDismissed } = this.props;

        const {
            dateCreated, dateUpdated, id, tableNumber
        } = check;

        const created = new Date(dateCreated);
        const updated = new Date(dateUpdated);

        return (
            <div className="active-check">
                <div className="active-check__header">
                    <button
                        className="active-check__dismiss"
                        type="button"
                        onClick={onCheckDismissed}
                    >
                        &times;
                    </button>
                    <div className="active-check__title">{id}</div>
                </div>
                <div className="active-check__info">
                    <div className="active-check__date-created">
                        <strong>Created: </strong>
                        {created.toLocaleTimeString()}
                    </div>
                    <div className="active-check__table">{`Table ${tableNumber}`}</div>
                    <div className="active-check__date-updated">
                        <strong>Updated: </strong>
                        {updated.toLocaleTimeString()}
                    </div>
                </div>
                <h4 className="active-check__item-heading">Ordered Items:</h4>
                {this.renderItems()}
                {check.tax !== null && (
                    <div className="active-check__tax">{`Tax: $${check.tax.toFixed(2)}`}</div>
                )}
                {check.tip !== null && (
                    <div className="active-check__tip">{`Tip: $${check.tip.toFixed(2)}`}</div>
                )}
                {!check.closed && (
                    <div className="active-check__close-button">
                        <button
                            className="close-button"
                            type="button"
                            onClick={() => onCheckClosed()}
                        >
                            Close out check
                        </button>
                    </div>
                )}
            </div>
        );
    }
}
