import React, { Component } from 'react';
import './NewCheckButton.css';

export default class NewCheckButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            tableSelect: false
        };
    }

    enableTableselect = () => {
        this.setState({ tableSelect: true });
    };

    disableTableselect = () => {
        this.setState({ tableSelect: false });
    };

    onTableClick = async (tableId) => {
        const { createNewCheck } = this.props;
        this.setState({ loading: true });
        await createNewCheck(tableId);
    };

    renderTables() {
        const { loading } = this.state;
        const { bookedTables, tables } = this.props;

        const text = tables.length === bookedTables.length ? 'No open tables' : 'Select a table';

        return (
            <div className="tables">
                {!loading && <div className="tables__title">{text}</div>}
                {loading && <div className="tables__title">Fetching check...</div>}
                <div className="tables__buttons">
                    {tables.map((table) => {
                        const disabled = bookedTables.includes(table.number) || loading;
                        return (
                            <button
                                className="table-button"
                                key={`table-${table.number}`}
                                type="button"
                                disabled={disabled}
                                onClick={() => this.onTableClick(table.id)}
                            >
                                {table.number}
                            </button>
                        );
                    })}
                </div>
                <button
                    className="cancel-check-button"
                    type="button"
                    onClick={this.disableTableselect}
                >
                    Cancel
                </button>
            </div>
        );
    }

    render() {
        const { tableSelect } = this.state;

        return (
            <div className="new-check">
                <button className="new-check-button" type="button" onClick={this.enableTableselect}>
                    New Check
                </button>
                {tableSelect && this.renderTables()}
            </div>
        );
    }
}
