import React, { Component } from 'react';
import './Items.css';

export default class Items extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false
        };
    }

    onItemClicked = async (itemId) => {
        const { loading } = this.state;
        const { addItem, fetchCheck } = this.props;
        if (loading) {
            console.log('Adding item in progress...');
            return;
        }
        this.setState({ loading: true });
        await addItem(itemId);
        await fetchCheck();
        this.setState({ loading: false });
    };

    renderItem(item) {
        const { loading } = this.state;
        const className = loading ? 'item loading' : 'item';
        return (
            <div
                className={className}
                key={`item-${item.id}`}
                onClick={() => this.onItemClicked(item.id)}
            >
                <div className="item__icon">+</div>
                <div className="item__text">
                    <div className="item__name">{item.name.toLowerCase()}</div>
                    <div className="item__price">{`$${item.price.toFixed(2)}`}</div>
                </div>
            </div>
        );
    }

    render() {
        const { showItems, items } = this.props;
        const className = showItems ? 'items active' : 'items';
        return (
            <div className={className}>
                <h2 className="items__title">Add Items</h2>
                {items && items.map(item => this.renderItem(item))}
            </div>
        );
    }
}
