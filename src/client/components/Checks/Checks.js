import React, { Component } from 'react';
import './Checks.css';

export default class Checks extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filterText: ''
        };
    }

    onChange = (event) => {
        const text = event.target.value;
        this.setState({
            filterText: text
        });
    };

    clearFilter = () => {
        this.setState({
            filterText: ''
        });
    };

    renderCheck(check) {
        const { activeCheck, setActiveCheck } = this.props;
        const dateCreated = new Date(check.dateCreated);
        const dateUpdated = new Date(check.dateUpdated);

        let className = activeCheck === check.id ? 'check active' : 'check';
        className += check.closed ? ' closed' : ' open';

        const dateOptions = {
            month: 'short',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric'
        };

        const checkDate = check.closed
            ? `Closed: ${dateUpdated.toLocaleString('en-US', dateOptions)}`
            : `Opened: ${dateCreated.toLocaleString('en-US', dateOptions)}`;

        return (
            <div
                className={className}
                key={`check-${check.id}`}
                onClick={() => setActiveCheck(check.id)}
            >
                <div className="check__info">
                    <div className="check__id">{`${check.id.split('-')[0]}`}</div>
                    <div className="check__date">{checkDate}</div>
                </div>
                <div className="check__table">{check.tableNumber}</div>
            </div>
        );
    }

    render() {
        const { filterText } = this.state;
        const { closedChecks, openChecks } = this.props;

        const hasOpenChecks = openChecks && openChecks.length > 0;
        const hasClosedChecks = closedChecks && closedChecks.length > 0;

        const sortedOpenChecks = openChecks.slice();
        sortedOpenChecks.sort((a, b) => a.tableNumber - b.tableNumber);

        const filteredClosedChecks = closedChecks.filter(
            c => !filterText || `${c.tableNumber}` === filterText
        );

        return (
            <div className="checks">
                {hasOpenChecks && (
                    <div className="checks__open">
                        <h2 className="checks__title">Open Checks</h2>
                        {sortedOpenChecks.map(check => this.renderCheck(check))}
                    </div>
                )}
                {hasClosedChecks && (
                    <div className="checks__closed">
                        <h2 className="checks__title">Closed Checks</h2>
                        <div className="check-filter">
                            <div>Filter by table:</div>
                            <input
                                type="number"
                                value={filterText}
                                max={10}
                                min={1}
                                onChange={this.onChange}
                            />
                            {filterText && (
                                <button
                                    className="check-filter__clear-button"
                                    type="button"
                                    onClick={this.clearFilter}
                                >
                                    Clear ×
                                </button>
                            )}
                        </div>
                        {filteredClosedChecks.map(check => this.renderCheck(check))}
                    </div>
                )}
                {!(hasOpenChecks || hasClosedChecks) && (
                    <div className="checks__empty">No checks found</div>
                )}
            </div>
        );
    }
}
